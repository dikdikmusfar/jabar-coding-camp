// C O N T O H //
var sayHello = "Hello World!" 
console.log(" C O N T O H")
console.log(sayHello)
console.log(" ")



// N O . 1 //
var pertama = "saya sangat senang hari ini"
var kedua = "belajar javascript itu keren"
// J A W A B A N  S O A L  N O . 1 //
var pertamax = pertama.substr(0,4)
var pertamay = pertama.substr(12,6)
var keduax = kedua.substr(0,7)
var keduay = kedua.substr(8,10)
var keduaxy = keduay.toUpperCase()
var ketiga = pertamax + " " + pertamay + " " + keduax + " " + keduaxy
console.log(" J A W A B A N   N O . 1")
console.log(ketiga)
console.log(" ")




// N O . 2 //
var kataPertama = "10"
var kataKedua = "2"
var kataKetiga = "4"
var kataKeempat = "6"
// J A W A B A N  S O A L  N O . 2 //
var sepuluh = parseInt(kataPertama)
var dua = parseInt(kataKedua)
var empat = parseInt(kataKetiga)
var enam = parseInt(kataKeempat)
var duapuluhempat = sepuluh*dua+empat
console.log(" J A W A B A N   N O . 2")
console.log(duapuluhempat)
console.log(" ")



// N O . 3 //
var kalimat = 'wah javascript itu keren sekali'; 
// J A W A B A N  S O A L  N O . 3 //
var kataPertamax = kalimat.substring(0, 3); 
var kataKeduax = kalimat.substr(4,10); // do your own! 
var kataKetigax = kalimat.substr(15,3); // do your own! 
var kataKeempatx = kalimat.substr(19,5); // do your own! 
var kataKelimax = kalimat.substring(25); // do your own! 
console.log(" J A W A B A N   N O . 3")
console.log('Kata Pertama: ' + kataPertamax); 
console.log('Kata Kedua: ' + kataKeduax); 
console.log('Kata Ketiga: ' + kataKetigax); 
console.log('Kata Keempat: ' + kataKeempatx); 
console.log('Kata Kelima: ' + kataKelimax);