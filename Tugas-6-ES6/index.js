/* 
 S O A L  N O . 1 
 buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
*/
console.log("=========")
console.log("No. 1")
const luas = (panjang,lebar) => {
    let hasil = panjang * lebar
    console.log("panjang = " + panjang)
    console.log("lebar = " + lebar)
    console.log("Luas = " + hasil)
    let hakel = panjang + lebar
    console.log("Keliling = " + hakel)
}
luas(2,5)

/*
 S O A L  N O . 2
 Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
 const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
*/
console.log("=========")
console.log("No. 2")
const newFunction = (firstName, lastName) => {
    return{
        firstName,
        lastName,
        fullName(){
            console.log(firstName + " " + lastName)
        }
    }
}
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

/*
 S O A L   N O . 3
 Diberikan sebuah objek sebagai berikut:

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const address = newObject.address;
const hobby = newObject.hobby;
Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// Driver code
console.log(firstName, lastName, address, hobby)
*/
console.log("=========")
console.log("No. 3")
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName,lastName,address,hobby} = newObject

console.log(firstName, lastName, address, hobby)


/*
 S O A L   N O . 4
 Kombinasikan dua array berikut menggunakan array spreading ES6

 const west = ["Will", "Chris", "Sam", "Holly"]
 const east = ["Gill", "Brian", "Noel", "Maggie"]
 const combined = west.concat(east)
 //Driver Code
 console.log(combined)
*/
console.log("=========")
console.log("No. 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
console.log(combined)


/*
 S O A L   N O . 5
 sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
 const planet = "earth" 
 const view = "glass" 
 var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
*/
console.log("=========")
console.log("No. 5")
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(before)