// S O A L   N O . 1 //
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for(var n = 0; n < daftarHewan.length; n++){
    var min = n
    for (var j = n+1; j < daftarHewan.length; j++){
        if (daftarHewan[j] < daftarHewan[min]){
            min = j
        }
    }
    if (min !== n){
        var sekarang = daftarHewan[n];
        daftarHewan[n] = daftarHewan[min];
        daftarHewan[min] = sekarang;
    }
}
console.log("======================") 
console.log("Jawaban No . 1")
console.log("======================")
console.log(daftarHewan[0])
console.log(daftarHewan[1])
console.log(daftarHewan[2])
console.log(daftarHewan[3])
console.log(daftarHewan[4])
console.log(" ")





// S O A L   N O . 2 //
function introduce(x) {
    return "Nama saya " + x.name + ", umur saya " + x.age + " tahun, alamat saya di " + x.address + ", dan saya punya hobby yaitu " + x.hobby
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log("======================")
console.log("Jawaban No . 2")
console.log("======================")
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"
console.log(" ")



// S O A L    N O . 3 //
function hitung_huruf_vokal (str) {
    var vok = 'aiueoAIEUO'
    var hit = 0

    for (var x = 0; x < str.length; x++){
        if (vok.indexOf(str[x]) !== -1) {
            hit++
        }
    }
    return hit
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log("======================")
console.log("Jawaban No . 3")
console.log("======================")
console.log(hitung_1 , hitung_2) // 3 2
console.log(" ")



// S O A L   N O . 4 //
function hitung(x){
    var y = 2*x -2 
    return y
}
console.log("======================")
console.log("Jawaban No . 4")
console.log("======================")
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
